/** 
*  Transaction model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  Transaction_id: { type: Number, required: true },
  Transaction_type:{type: String, required: true},
  Transaction_date:{type: String, required: true},
  Transaction_amount: { type: Number, required: true }
})

module.exports = mongoose.model('Customer', CustomerSchema)
