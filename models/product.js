/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({
  Account_type: { type: String, required: true, default:'Checking' },
  Account_no: { type: Number, required: true, unique: true },
  Account_name: { type: String, required: true, }
})

module.exports = mongoose.model('Product', ProductSchema)
