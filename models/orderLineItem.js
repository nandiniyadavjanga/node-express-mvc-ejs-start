/** 
*  Order Line Item model
*  Describes the characteristics of each attribute in an order line item - one entry on a customer's order.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrderLineItemSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  categoryID: { type: Number, required: true },
  categoryType: { type: String, required: true },
  
  categoryAmount: { type: Number, required: true, default: 0 }
})

module.exports = mongoose.model('OrderLineItem', OrderLineItemSchema)
